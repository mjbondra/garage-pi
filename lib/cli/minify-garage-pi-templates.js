import { promises as fs } from "fs";
import { minify } from "html-minifier";
import path from "path";
import process from "process";

import minifyJS from "../../util/minify-js.js";
import module from "../../util/module.js";

const { __dirname } = module(import.meta.url);

(async () => {
  const directory = path.join(__dirname, "..", "..", "tpl");
  const files = await fs.readdir(directory);
  const minifications = files
    .filter(fileName => /^((?!min).)*\.html$/g.test(fileName))
    .map(async fileName => {
      const filePath = path.join(directory, fileName);
      const html = await fs.readFile(filePath, "utf8");
      const minifiedPath = filePath.replace(/\.html$/, ".min.html");
      const minifiedHTML = minify(html, {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: minifyJS
      });
      return fs.writeFile(minifiedPath, minifiedHTML, "utf8");
    });

  await Promise.all(minifications);

  process.stdout.write("garage pi templates minified\n");
  return process.exit(0);
})().catch(err => {
  process.stderr.write(`${err.message}\n`);
  return process.exit(1);
});
