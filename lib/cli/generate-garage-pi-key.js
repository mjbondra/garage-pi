import aws from "aws-sdk";
import { promises as fs } from "fs";
import path from "path";
import process from "process";
import yargs from "yargs";

import addActiveKey from "../../util/add-active-key.js";
import genRandomString from "../../util/get-random-string.js";
import hash from "../../util/hash.js";
import module from "../../util/module.js";

import config from "../config.js";

const { __dirname } = module(import.meta.url);

const {
  email,
  hostname,
  phone,
  port,
  protocol,
  title,
  webSocketProtocol
} = yargs
  .option("email", {
    alias: "e",
    describe: "email address",
    required: true
  })
  .option("hostname", {
    alias: "h",
    default: config.server.hostname,
    describe: "service hostname"
  })
  .option("phone", {
    alias: "p",
    describe: "phone number",
    required: true
  })
  .option("port", {
    default: config.server.port,
    describe: "service port"
  })
  .option("protocol", {
    default: config.server.protocol,
    describe: "service protocol"
  })
  .option("title", {
    alias: "t",
    default: config.name,
    describe: "title"
  })
  .option("web-socket-protocol", {
    default: config.server.webSocketProtocol,
    describe: "web socket protocol"
  }).argv;

(async () => {
  const ses = new aws.SES({ region: config.aws.region });

  const formattedPhoneNumber = `${phone}`
    .replace(/[ ()-]/g, "")
    .replace(/^\+*1*/, "+1");

  if (
    !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      email
    )
  ) {
    throw new Error(`email address is invalid: ${email}`);
  }

  if (!/^\+1[0-9]{10}$/.test(formattedPhoneNumber)) {
    throw new Error(`phone number is invalid: ${phone}`);
  }

  const id = genRandomString(16);
  const salt = genRandomString(16);
  const token = genRandomString(32);
  const basicAuthToken = Buffer.from(`${id}:${token}`).toString("base64");

  await addActiveKey(
    Buffer.from(
      JSON.stringify({
        id,
        email,
        phone: formattedPhoneNumber,
        ...hash(token, salt)
      })
    ).toString("base64")
  );

  const templatesDirectory = path.join(__dirname, "..", "..", "tpl");
  const authHTMLTemplate = await fs.readFile(
    path.join(templatesDirectory, "auth.min.html"),
    "utf8"
  );

  const authHTML = authHTMLTemplate
    .replace(/{{ TITLE }}/g, title)
    .replace(/{{ AUTHORIZATION }}/g, `BASIC ${basicAuthToken}`)
    .replace(/{{ HOSTNAME }}/g, hostname)
    .replace(/{{ PORT }}/g, port)
    .replace(/{{ PROTOCOL }}/g, protocol)
    .replace(/{{ WEB_SOCKET_PROTOCOL }}/g, webSocketProtocol);

  const base64HTML = Buffer.from(authHTML).toString("base64");

  const emailHTMLTemplate = await fs.readFile(
    path.join(templatesDirectory, "token-interface-email.min.html"),
    "utf8"
  );

  const emailHTML = emailHTMLTemplate
    .replace(/{{ TITLE }}/g, title)
    .replace(/{{ BASE64_HTML }}/g, base64HTML);

  await ses
    .sendEmail({
      Destination: {
        ToAddresses: [email]
      },
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: emailHTML
          }
        },
        Subject: {
          Charset: "UTF-8",
          Data: `${title} Token Interface`
        }
      },
      Source: config.email.sender
    })
    .promise();

  process.stdout.write(`${title} Token Interface sent to: ${email}\n`);
  return process.exit(0);
})().catch(err => {
  process.stderr.write(`${err.message}\n`);
  return process.exit(1);
});
