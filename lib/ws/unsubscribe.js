import state from "../state.js";

const wsUnsubscribe = connection => {
  state.webSocketConnections = state.webSocketConnections.filter(
    stateConnection => stateConnection !== connection
  );
};

export default wsUnsubscribe;
