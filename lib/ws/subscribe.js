import process from "process";
import { promisify } from "util";

import isValidAuthToken from "../../util/is-valid-auth-token.js";

import contact from "../contact.js";
import state from "../state.js";

const contactReadAsync = promisify(contact.read).bind(contact);

const wsSubscribe = async (connection, message) => {
  try {
    if (!(await isValidAuthToken(message.utf8Data)))
      throw new Error("Unauthorized");
    if (!state.webSocketConnections.includes(connection))
      state.webSocketConnections = [...state.webSocketConnections, connection];
    connection.sendUTF(await contactReadAsync());
  } catch (err) {
    process.stderr.write("failed to authorize web socket connection");
    state.webSocketConnections = state.webSocketConnections.filter(
      stateConnection => stateConnection !== connection
    );
    connection.drop(1007);
  }
};

export default wsSubscribe;
