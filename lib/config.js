import dotenv from "dotenv";
import fs from "fs";
import path from "path";
import process from "process";
import yaml from "js-yaml";

import configToEnv from "../util/config-to-env.js";
import envToConfig from "../util/env-to-config.js";
import module from "../util/module.js";

const { __dirname } = module(import.meta.url);

const configFilePath = path.join(__dirname, "..", "conf", "config.yml");
const envFilePath = path.join(__dirname, "..", ".env");

try {
  fs.accessSync(envFilePath, fs.constants.F_OK | fs.constants.R_OK);
} catch (err) {
  if (err.code !== "ENOENT") throw err;
  process.stdout.write(`creating env file\n`);
  fs.writeFileSync(envFilePath, "\n", "utf8");
}

const defaultConfig = yaml.safeLoad(fs.readFileSync(configFilePath, "utf8"));
const env = dotenv.parse(fs.readFileSync(envFilePath, "utf8"));

export default envToConfig({
  ...configToEnv(defaultConfig),
  ...process.env,
  ...env
});
