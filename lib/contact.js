import { Gpio } from "onoff";
import process from "process";

import setContactAlarm from "../util/set-contact-alarm.js";

import config from "./config.js";
import state from "./state.js";

const contact = new Gpio(config.gpio.contact, "in", "both");

contact.watch((watchErr, val) => {
  setContactAlarm(val);
  process.stdout.write(`${new Date()} - contact state change: ${val}\n`);
  try {
    if (watchErr) throw watchErr;
    state.webSocketConnections
      .filter(connection => connection.connected)
      .forEach(connection => connection.sendUTF(val));
  } catch (err) {
    process.stderr.write(`${err.stack}\n`);
  }
});

export default contact;
