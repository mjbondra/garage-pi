import config from "../config.js";

const httpMethodNotAllowed = (req, res) => {
  res.writeHead(405, {
    Allow: "POST",
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: null,
      error: `${new Error("Method Not Allowed")}`
    })
  );
  return res.end();
};

export default httpMethodNotAllowed;
