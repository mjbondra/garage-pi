import config from "../config.js";

const httpInternalServerError = (req, res) => {
  res.writeHead(500, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: null,
      error: `${new Error("Internal Server Error")}`
    })
  );
  return res.end();
};

export default httpInternalServerError;
