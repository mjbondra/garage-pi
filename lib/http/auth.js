import aws from "aws-sdk";

import generateRelayCode from "../../util/generate-relay-code.js";
import getUnpackedKey from "../../util/get-unpacked-key.js";

import config from "../config.js";
import state from "../state.js";

const ses = new aws.SES({ region: config.aws.region });
// const sns = new SNS({ region: config.aws.region });

const httpAuth = async (req, res) => {
  const { email } = await getUnpackedKey(req.headers.authorization);

  state.relayCode = generateRelayCode();

  const message = `GaragePi: Your relay code is: ${state.relayCode}`;

  // await sns
  //   .publish({
  //     Message: message,
  //     PhoneNumber: phone
  //   })
  //   .promise();

  await ses
    .sendEmail({
      Destination: {
        ToAddresses: [email]
      },
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: message
          }
        },
        Subject: {
          Charset: "UTF-8",
          Data: "GaragePi Relay Code"
        }
      },
      Source: config.email.sender
    })
    .promise();

  res.writeHead(201, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: "sent relay code",
      error: null
    })
  );
  return res.end();
};

export default httpAuth;
