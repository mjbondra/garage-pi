import config from "../config.js";

const httpOptions = (req, res) => {
  res.writeHead(204, {
    ...config.headers.CORS
  });
  return res.end();
};

export default httpOptions;
