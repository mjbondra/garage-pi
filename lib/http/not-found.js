import config from "../config.js";

const httpNotFound = (req, res) => {
  res.writeHead(404, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: null,
      error: `${new Error("Not Found")}`
    })
  );
  return res.end();
};

export default httpNotFound;
