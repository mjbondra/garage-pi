import config from "../config.js";

const httpUnauthorized = (req, res) => {
  res.writeHead(401, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({ data: null, error: `${new Error("Unauthorized")}` })
  );
  res.end();
};

export default httpUnauthorized;
