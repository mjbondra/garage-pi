import { Gpio } from "onoff";
import process from "process";
import { promisify } from "util";
import setTimeoutAsync from "../../util/set-timeout-async.js";
import state from "../state.js";
import unauthorizedHandler from "./unauthorized.js";

import config from "../config.js";

const defaultRelay = new Gpio(config.gpio.relay, "out");

const httpRelay = async (req, res, relay = defaultRelay) => {
  const relayWriteAsync = promisify(relay.write).bind(relay);
  const { "relay-code": relayCode } = req.headers;
  if (!relayCode || relayCode !== `${state.relayCode}`)
    return unauthorizedHandler(req, res);

  await relayWriteAsync(1);
  await setTimeoutAsync(config.gpio.relayTimeout);
  await relayWriteAsync(0);
  process.stdout.write(`${new Date()} - relay triggered\n`);

  state.relayCode = null;
  res.writeHead(201, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: "relay triggered",
      error: null
    })
  );
  return res.end();
};

export default httpRelay;
