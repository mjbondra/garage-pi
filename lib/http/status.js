import config from "../config.js";

const STATUS = {
  ERROR: "error",
  OKAY: "okay",
  WARN: "warn"
};

const httpStatus = (req, res) => {
  res.writeHead(200, {
    ...config.headers.CORS,
    ...config.headers.JSON
  });
  res.write(
    JSON.stringify({
      data: { status: STATUS.OKAY },
      error: null
    })
  );
  return res.end();
};

export default httpStatus;
