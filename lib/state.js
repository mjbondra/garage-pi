import config from "./config.js";

const data = {
  relayCode: null,
  relayCodeTimeout: null,
  webSocketConnections: []
};

export default {
  get relayCode() {
    return data.relayCode;
  },
  set relayCode(relayCode) {
    if (data.relayCodeTimeout) clearTimeout(data.relayCodeTimeout);
    if (relayCode) {
      data.relayCode = relayCode;
      data.relayCodeTimeout = setTimeout(() => {
        data.relayCode = null;
        data.relayCodeTimeout = null;
      }, config.relayCode.ttl);
    } else {
      data.relayCode = null;
      data.relayCodeTimeout = null;
    }
  },
  get webSocketConnections() {
    return [...data.webSocketConnections];
  },
  set webSocketConnections(webSocketConnections) {
    data.webSocketConnections = webSocketConnections.filter(
      connection => connection.connected
    );
  }
};
