import http from "http";
import process from "process";
import { server as WebSocketServer } from "websocket";

import httpAuth from "./http/auth.js";
import httpInternalServerError from "./http/internal-server-error.js";
import httpMethodNotAllowed from "./http/method-not-allowed.js";
import httpNotFound from "./http/not-found.js";
import httpOptions from "./http/options.js";
import httpRelay from "./http/relay.js";
import httpStatus from "./http/status.js";
import httpUnauthorized from "./http/unauthorized.js";

import wsSubscribe from "./ws/subscribe.js";
import wsUnsubscribe from "./ws/unsubscribe.js";

import isValidAuthToken from "../util/is-valid-auth-token.js";

import config from "./config.js";

const {
  server: { hostname, port, protocol }
} = config;

const server = http.createServer(async (req, res) => {
  process.stdout.write(`${new Date()} - ${req.method} ${req.url}\n`);
  try {
    switch (req.method) {
      case "OPTIONS":
        return httpOptions(req, res);

      case "GET":
        switch (req.url.replace(/\/$/, "")) {
          case "/status":
            return httpStatus(req, res);
          default:
            return httpNotFound(req, res);
        }

      case "POST":
        if (!(await isValidAuthToken(req.headers.authorization)))
          return httpUnauthorized(req, res);
        switch (req.url.replace(/\/$/, "")) {
          case "/auth":
            return httpAuth(req, res);
          case "/relay":
            return httpRelay(req, res);
          default:
            return httpNotFound(req, res);
        }

      default:
        return httpMethodNotAllowed(req, res);
    }
  } catch (err) {
    process.stderr.write(`${err.stack}\n`);
    return httpInternalServerError(req, res);
  }
});

const webSocketServer = new WebSocketServer({
  autoAcceptConnections: false,
  httpServer: server
});

webSocketServer.on("request", async req => {
  try {
    if (req.origin !== "null") {
      req.reject(403, "Forbidden");
      return;
    }

    const connection = req.accept("garage-pi-protocol", req.origin);
    connection.on("message", message => wsSubscribe(connection, message));
    connection.on("close", () => wsUnsubscribe(connection));
  } catch (err) {
    process.stderr.write(`${err.stack}\n`);
    req.reject(500, "Internal Server Error");
  }
});

server.listen(port, () =>
  process.stdout.write(`listening on: ${protocol}://${hostname}:${port}\n`)
);
