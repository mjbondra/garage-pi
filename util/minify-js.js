import process from "process";
import Terser from "terser";

const minifyJS = (input, inline) => {
  const { code, error } = Terser.minify(input, {
    parse: { bare_returns: inline }
  });
  if (error) {
    process.stderr.write(`${error.message}\n`);
    return input;
  }
  return code.replace(/;$/, "");
};

export default minifyJS;
