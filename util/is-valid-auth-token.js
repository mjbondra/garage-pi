import process from "process";

import getActiveKeys from "./get-active-keys.js";
import hash from "./hash.js";

const isValidAuthToken = async authToken => {
  try {
    const [id, token] = Buffer.from(authToken.replace("BASIC ", ""), "base64")
      .toString()
      .split(":");

    if (!id || !token) throw new Error("invalid or expired auth token");

    const { hash: expectedHash, salt } =
      (await getActiveKeys())
        .map(key => JSON.parse(Buffer.from(key, "base64").toString()))
        .find(data => data.id === id) || {};

    if (!expectedHash || !salt)
      throw new Error("invalid or expired auth token");

    const { hash: actualHash } = hash(token, salt);

    if (expectedHash !== actualHash)
      throw new Error("invalid or expired auth token");

    return true;
  } catch (err) {
    process.stderr.write(`${err.message}\n`);
    return false;
  }
};

export default isValidAuthToken;
