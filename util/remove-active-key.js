import { promises as fs } from "fs";
import path from "path";

import config from "../lib/config.js";
import getActiveKeys from "./get-active-keys.js";
import module from "./module.js";

const { __dirname } = module(import.meta.url);

const removeActiveKey = async (
  id,
  activeKeysPath = path.resolve(__dirname, "..", config.keys.path)
) => {
  try {
    const keys = await getActiveKeys();
    return fs.writeFile(
      activeKeysPath,
      `${keys
        .map(key => JSON.parse(Buffer.from(key, "base64").toString()))
        .filter(data => data.id !== id)
        .map(data => Buffer.from(JSON.stringify(data)).toString("base64"))
        .join("\n")}\n`,
      "utf8"
    );
  } catch (err) {
    process.stderr.write("failed to remove active key\n");
    process.stderr.write(`${err.stack}\n`);
    throw err;
  }
};

export default removeActiveKey;
