import { promises as fs } from "fs";
import path from "path";

import config from "../lib/config.js";
import module from "./module.js";

const { __dirname } = module(import.meta.url);

const addActiveKey = async (
  key,
  activeKeysPath = path.resolve(__dirname, "..", config.keys.path)
) => fs.appendFile(activeKeysPath, `${key}\n`);

export default addActiveKey;
