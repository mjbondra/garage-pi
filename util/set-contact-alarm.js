import aws from "aws-sdk";

import config from "../lib/config.js";

import getActiveKeys from "./get-active-keys.js";

const ses = new aws.SES({ region: config.aws.region });

const state = {
  interval: null,
  openedAt: null
};

const setContactAlarm = val => {
  if (state.interval) clearInterval(state.interval);
  if (val !== 1) return (state.interval = null);
  state.openedAt = new Date();
  process.stdout.write(
    `${new Date()} - alarm interval set for ${config.alarm.interval}ms\n`
  );

  return (state.interval = setInterval(async () => {
    const durationOpenedInMinutes = Math.floor(
      (new Date() - state.openedAt) / 60000
    );

    const message = `GaragePi: ALARM! Open for ${durationOpenedInMinutes} minutes.`;
    const keys = await getActiveKeys();

    return Promise.all(
      keys
        .map(key => JSON.parse(Buffer.from(key, "base64").toString()))
        .map(user =>
          ses
            .sendEmail({
              Destination: {
                ToAddresses: [user.email]
              },
              Message: {
                Body: {
                  Html: {
                    Charset: "UTF-8",
                    Data: message
                  }
                },
                Subject: {
                  Charset: "UTF-8",
                  Data: "GaragePi Alarm"
                }
              },
              Source: config.email.sender
            })
            .promise()
        )
      // .map(user =>
      //   sns
      //     .publish({
      //       Message: message,
      //       PhoneNumber: user.phone
      //     })
      //     .promise()
      // )
    ).catch(err => {
      process.stderr.write(`${err.stack}\n`);
      return null;
    });
  }, config.alarm.interval));
};

export default setContactAlarm;
