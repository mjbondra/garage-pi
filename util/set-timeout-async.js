const setTimeoutAsync = async delay =>
  new Promise(resolve => setTimeout(resolve, delay));

export default setTimeoutAsync;
