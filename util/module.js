import { dirname } from "path";
import { fileURLToPath } from "url";

const module = url => {
  const filename = fileURLToPath(url);
  return {
    __dirname: dirname(filename),
    __filename: filename
  };
};

export default module;
