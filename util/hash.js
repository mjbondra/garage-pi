import crypto from "crypto";

/**
 * hash password with sha512.
 *
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
const hash = (password, salt) => {
  const hmac = crypto.createHmac(
    "sha512",
    salt
  ); /** Hashing algorithm sha512 */

  hmac.update(password);

  return {
    salt,
    hash: hmac.digest("hex")
  };
};

export default hash;
