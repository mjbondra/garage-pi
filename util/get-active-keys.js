import { promises as fs } from "fs";
import path from "path";
import process from "process";

import config from "../lib/config.js";
import module from "./module.js";

const { __dirname } = module(import.meta.url);

const getActiveKeys = async (
  activeKeysPath = path.resolve(__dirname, "..", config.keys.path)
) => {
  try {
    const keys = await fs.readFile(activeKeysPath, "utf8");
    return keys.split("\n").filter(key => key);
  } catch (err) {
    if (err.code !== "ENOENT") throw err;
    process.stderr.write("no active keys -- ");
    process.stderr.write("execute `generate-garage-pi-key` to create one\n");
    return [];
  }
};

export default getActiveKeys;
