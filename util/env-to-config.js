/**
 * convert a flat environment variables object into an expanded configuration object
 *
 * @param {object} [env=process.env] - flat environment variables object
 * @return {object} expanded configuration object
 */
const envToConfig = (env = process.env) =>
  Object.entries(
    Object.entries(env).reduce((conf, [key, value]) => {
      const [base, ...nested] = key.split("__");
      return {
        ...conf,
        [base]: nested.length
          ? { ...conf[base], [nested.join("__")]: value }
          : value
      };
    }, {})
  ).reduce((conf, [key, value]) => {
    if (typeof value === "object")
      return { ...conf, [key]: envToConfig(value) };
    try {
      return { ...conf, [key]: JSON.parse(value) };
    } catch (err) {
      return { ...conf, [key]: value };
    }
  }, {});

export default envToConfig;
