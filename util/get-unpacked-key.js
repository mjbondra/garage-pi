import getActiveKeys from "./get-active-keys.js";

const getUnpackedKey = async authToken => {
  const [id] = Buffer.from(authToken.replace("BASIC ", ""), "base64")
    .toString()
    .split(":");

  return (
    (await getActiveKeys())
      .map(key => JSON.parse(Buffer.from(key, "base64").toString()))
      .find(data => data.id === id) || {}
  );
};

export default getUnpackedKey;
