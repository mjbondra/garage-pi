/**
 * convert an expanded configuration object into a flat environment variables object
 *
 * @param {object} config - configuration object
 * @param {string} [prefix=""] - prefix for envrionment variable keys
 * @return {object} flattened environment variables object with previous nesting delimited by "__" in variable keys
 */
const configToEnv = (config, prefix = "") =>
  Object.entries(config).reduce((env, [key, value]) => {
    const prefixedKey = prefix ? `${prefix}__${key}` : key;
    if ([undefined, null].includes(value)) return { ...env, [prefixedKey]: "" };
    if (Array.isArray(value)) {
      if (value.every(item => typeof item !== "object" || item instanceof Date))
        return {
          ...env,
          [prefixedKey]: JSON.stringify(value)
        };
      throw new Error(
        `arrays of objects not supported in configuration: ${prefixedKey}`
      );
    }
    if (typeof value !== "object" || value instanceof Date)
      return { ...env, [prefixedKey]: `${value}` };
    return { ...env, ...configToEnv(value, prefixedKey) };
  }, {});

export default configToEnv;
