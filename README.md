# @mjbondra/garage-pi

Simple SMS authorization utility for triggering a relay by toggling a GPIO pin on a supported device (Raspberry Pi, C.H.I.P., BeagleBone, etc).

I use it to open and close my garage.

## Requirements

In additional to requiring execution on a device with GPIO pins, this application requires access to [AWS SNS](https://aws.amazon.com/sns/) (`sns:Publish`) to send an SMS message for authorization. It also requires configuration and access to [AWS SES](https://aws.amazon.com/ses/) (`ses:SendEmail`) to send token interface information to new users.

## License

Copyright (c) 2019 Michael J. Bondra <mjbondra@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
